{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";

    devshell = {
      url = "github:numtide/devshell";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs = { self, devshell, flake-utils, rust-overlay, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ devshell.overlays.default (import rust-overlay) ];
        pkgs = import nixpkgs { inherit overlays system; };
        rust = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
        rust-dev = rust.override { extensions = [ "rust-src" "rust-analyzer" "llvm-tools-preview" ]; };
      in rec {
        devShell = pkgs.devshell.mkShell {
          name = "skavos-dev";

          commands = [{
            name = "coverage";
            command = let
              excl_enum_struct = "^([[:space:]]*)(pub |pub(([[:alpha:]]|[[:space:]]|[:])+) )?(enum|struct) [^\\{]*\\{";
              excl_enum_fn_struct = "^([[:space:]]*)(pub |pub(([[:alpha:]]|[[:space:]]|[:])+) )?(enum|(const )?fn|struct) ";
              excl_line = "//!|#\\[|use|unreachable!|^\\}$|${excl_enum_struct}";
              excl_start = "#\\[no_coverage\\]|${excl_enum_struct}";
              excl_stop = "^\\}$";
              excl_br_line = "#\\[|assert(_eq)?!|(error|warn|info|debug|trace)!|^[[:space:]]*\\}(,)?$|${excl_enum_fn_struct}";
              excl_br_start = "#\\[no_coverage\\]|^mod tests \\{|${excl_enum_struct}";
              excl_br_stop = "^\\}$";
              env = "CARGO_INCREMENTAL=0"
                  + " RUSTFLAGS=\"-Zno-profiler-runtime -Cinstrument-coverage\""
                  + " RUSTDOCFLAGS=\"-Cpanic=abort\"";
            in ''
              ${env} cargo test
              grcov target/output.profraw -s . -b ./target/x86_64/debug/ --llvm --ignore-not-existing \
                  --ignore '*cargo/*' --ignore '*alloc/*' --ignore '*core/*' --ignore '*portable-simd/*' --ignore '*stdarch/*' --ignore '*target/*' \
                  --excl-line "${excl_line}" --excl-start "${excl_start}" --excl-stop "${excl_stop}" \
                  --excl-br-line "${excl_br_line}" --excl-br-start "${excl_br_start}" --excl-br-stop "${excl_br_stop}" \
                  -o ./target/coverage.lcov
              find target \( -name "*.gcda" -or -name "*.gcno" \) -delete
              genhtml --no-function-coverage --precision 2 target/coverage.lcov -o coverage
            '';
            help = "Launch tests and generate HTML coverage website";
          }];

          packages = with pkgs; [ cargo-deny clang_15 grcov lcov qemu rust-dev ];
        };
      });
}
