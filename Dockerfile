FROM    debian:bookworm-slim
ARG	RUST_TOOLCHAIN=nightly-2023-04-30

RUN     apt-get update && apt-get install -y build-essential clang curl git grcov lcov openssh-client rsync qemu-system-x86

RUN     curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
ENV     PATH="/root/.cargo/bin:${PATH}"

RUN     rustup toolchain install ${RUST_TOOLCHAIN} --component clippy llvm-tools-preview rust-src rustfmt && \
	rustup target add x86_64-unknown-none --toolchain ${RUST_TOOLCHAIN}

RUN	cargo install cargo-deny

RUN     git clone https://gitlab.crans.org/v-lafeychine/skavos-bootimage && \
        	cd skavos-bootimage && cargo install --path .
