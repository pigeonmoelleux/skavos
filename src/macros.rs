//! Implementation of macros that can be used everywhere

use core::fmt::{Arguments, Write};

use x86_64::instructions::interrupts;

/// Used to define the macro rules `print!` and `println!`
#[doc(hidden)]
#[inline]
pub fn _print(args: Arguments) {
    interrupts::without_interrupts(|| {
        #[cfg(not(test))]
        crate::kernel::screen::buffer::WRITER
            .lock()
            .get_mut()
            .expect("The writer has not been initialized yet")
            .write_fmt(args)
            .unwrap_or_else(|_| unreachable!("Framebuffer is assumed to working correctly"));

        #[cfg(test)]
        crate::test::SERIAL1
            .lock()
            .write_fmt(args)
            .unwrap_or_else(|_| unreachable!("COM1 serial port is assumed to working correctly"));
    });
}

/// Writes formatted text on the screen
#[macro_export]
macro_rules! print {
    (color: $color:expr, $($arg:tt)+) => {
        $crate::kernel::screen::buffer::WRITER.
            lock()
            .get_mut()
            .expect("The writer has not been initialized yet")
            .set_color($color);

        $crate::macros::_print(format_args!($($arg)+));

        $crate::kernel::screen::buffer::WRITER
            .lock()
            .get_mut()
            .expect("The writer has not been initialized yet")
            .set_color($crate::kernel::screen::color::Color::default());
    };
    ($($arg:tt)+) => {
        $crate::macros::_print(format_args!($($arg)+))
    };
}

/// Writes formatted text on the screen then appends a new line
#[macro_export]
macro_rules! println {
    () => {
        $crate::print!("\n")
    };
    (color: $color:expr, $($arg:tt)+) => {
        $crate::print!(color: $color, "{}\n", format_args!($($arg)+))
    };
    ($($arg:tt)+) => {
        $crate::print!("{}\n", format_args!($($arg)+))
    };
}
