//! Implementation of tests without `std`

use qemu_exit::QEMUExit;
use spin::{Lazy, Mutex};
use uart_16550::SerialPort;

use crate::{print, println};

/// First serial communication port
///
/// Uses for traditional standard output
pub static SERIAL1: Lazy<Mutex<SerialPort>> = Lazy::new(|| {
    // SAFETY: This call is safe since the port used is the standard port number of the first serial communication port
    // See more informations on the [OSdev wiki](https://wiki.osdev.org/Serial_Ports#Port_Addresses)
    let mut serial_port = unsafe { SerialPort::new(0x3F8) };
    serial_port.init();
    Mutex::new(serial_port)
});

/// Third serial communication port
///
/// Uses for coverage file transfer
pub static SERIAL3: Lazy<Mutex<SerialPort>> = Lazy::new(|| {
    // SAFETY: This call is safe since the port used is the standard port number of the third serial communication port
    // See more informations on : [OSdev wiki](https://wiki.osdev.org/Serial_Ports#Port_Addresses)
    let mut serial_port = unsafe { SerialPort::new(0x3E8) };
    serial_port.init();
    Mutex::new(serial_port)
});

/// Trait that every tested function implement
pub trait Testable {
    /// Run the function with a pretty printing
    fn run(&self);
}

impl<T> Testable for T
where
    T: Fn(),
{
    #[inline]
    fn run(&self) {
        print!("{:.<70}", core::any::type_name::<T>());
        self();
        println!("[ok]");
    }
}

/// Runner for the tests
///
/// This function will be called only if `cargo test` is invoked.
pub fn runner(tests: &[&dyn Testable]) {
    println!("Running {} tests", tests.len());

    for test in tests {
        test.run();
    }

    generate_coverage();
}

/// `QEmu` exit wrapper
pub fn exit() -> ! {
    let qemu_exit_handle = qemu_exit::X86::new(0xf4, 13);
    qemu_exit_handle.exit_success();
}

/// Generate coverage report through COM2 serial port
fn generate_coverage() {
    let mut coverage = alloc::vec![];

    // SAFETY: This code is not thread-safe but only one call is made.
    unsafe {
        minicov::capture_coverage(&mut coverage).unwrap();
    };

    let mut serial_port = SERIAL3.lock();

    coverage.iter().for_each(|&b| serial_port.send_raw(b));
}
