//! Simple Terminal UI for the user-space

use futures_util::StreamExt;

use crate::kernel::interrupt::keyboard::KeyQueue;
use crate::print;

/// Handles an key received
pub async fn print_key() {
    let mut key_queue = KeyQueue::new();

    while let Some(c) = key_queue.next().await {
        print!("{}", c);
    }
}
