//! Mutex providing mutually exclusive access to data
//!
//! Is used to make sure two alloc/dealloc are run at the same time

use derive_more::{Deref, DerefMut};
use spin::Mutex;

/// Structure that can be locked to ensure mutually exclusive access to data
#[derive(Deref, DerefMut)]
pub struct Locked<A>(Mutex<A>);

impl<A> Locked<A> {
    /// Creates a new locked structure from an existing one
    pub const fn new(inner: A) -> Self {
        Self(Mutex::new(inner))
    }
}
