//! Everything related to the screen output, principally the frame buffer

pub mod buffer;
pub mod color;
