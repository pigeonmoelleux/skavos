//! Colors displayed on screen output

use bootloader_api::info::PixelFormat;

/// Structure used to uniformize colors
///
/// The encoding used is RGB
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Color(u8, u8, u8);

/// White color
pub const WHITE: Color = Color(u8::MAX, u8::MAX, u8::MAX);

/// Black color
#[allow(unused)]
pub const BLACK: Color = Color(u8::MIN, u8::MIN, u8::MIN);

/// Red color
pub const RED: Color = Color(u8::MAX, u8::MIN, u8::MIN);

/// Green color
#[allow(unused)]
pub const GREEN: Color = Color(u8::MIN, u8::MAX, u8::MIN);

/// Blue color
#[allow(unused)]
pub const BLUE: Color = Color(u8::MIN, u8::MIN, u8::MAX);

/// Cyan color
pub const CYAN: Color = Color(u8::MIN, u8::MAX, u8::MAX);

/// Yellow color
pub const YELLOW: Color = Color(u8::MAX, u8::MAX, u8::MIN);

/// Orange color
pub const ORANGE: Color = Color(u8::MAX, 0xA5, u8::MIN);

impl Default for Color {
    fn default() -> Self {
        WHITE
    }
}

impl Color {
    /// Returns the red component of the color
    const fn red(self) -> u8 {
        self.0
    }

    /// Returns the green component of the color
    const fn green(self) -> u8 {
        self.1
    }

    /// Returns the blue component of the color
    const fn blue(self) -> u8 {
        self.2
    }

    /// Creates a new color resulting from the treatment of the intensity
    #[allow(clippy::cast_possible_truncation)]
    #[allow(clippy::integer_division)]
    pub fn with_intensity(self, intensity: u8) -> Self {
        Self(
            (u16::from(self.0) * u16::from(intensity) / u16::from(u8::MAX)) as u8,
            (u16::from(self.1) * u16::from(intensity) / u16::from(u8::MAX)) as u8,
            (u16::from(self.2) * u16::from(intensity) / u16::from(u8::MAX)) as u8,
        )
    }

    /// Gives the gray shade with a u8 value equals to the average of the RGB colors
    #[allow(clippy::integer_division)]
    const fn gray(self) -> u8 {
        self.red() / 3 + self.blue() / 3 + self.green() / 3
    }

    /// Returns the bytes needed for the encoding of the color in the frame buffer
    ///
    /// It is assumed here that pixels are not encoded on more than 4 bytes
    pub fn to_bytes(self, pixel_format: PixelFormat) -> [u8; 4] {
        match pixel_format {
            PixelFormat::Rgb => [self.red(), self.green(), self.blue(), 0],
            PixelFormat::Bgr => [self.blue(), self.green(), self.red(), 0],
            PixelFormat::U8 => [self.gray(), 0, 0, 0],
            PixelFormat::Unknown { .. } | _ => panic!("Pixel color format not taken in charge!"),
        }
    }
}

#[cfg(test)]
mod test {
    use bootloader_api::info::PixelFormat;

    use super::{Color, BLUE, GREEN, RED, WHITE};

    #[test_case]
    fn red() {
        assert_eq!(RED.red(), u8::MAX);
        assert_eq!(GREEN.red(), u8::MIN);
    }

    #[test_case]
    fn green() {
        assert_eq!(GREEN.green(), u8::MAX);
        assert_eq!(BLUE.green(), u8::MIN);
    }

    #[test_case]
    fn blue() {
        assert_eq!(BLUE.blue(), u8::MAX);
        assert_eq!(RED.blue(), u8::MIN);
    }

    #[test_case]
    fn intensity() {
        assert_eq!(GREEN.with_intensity(u8::MAX / 2), Color(u8::MIN, u8::MAX / 2, u8::MIN));
        assert_eq!(WHITE.with_intensity(100), Color(100, 100, 100));
    }

    #[test_case]
    fn gray() {
        assert_eq!(GREEN.gray(), u8::MAX / 3);
        assert_eq!(WHITE.gray(), u8::MAX);
    }

    #[test_case]
    fn to_bytes() {
        assert_eq!(GREEN.to_bytes(PixelFormat::Rgb), [0, 255, 0, 0]);
        assert_eq!(BLUE.to_bytes(PixelFormat::Bgr), [255, 0, 0, 0]);
        assert_eq!(RED.to_bytes(PixelFormat::U8), [u8::MAX / 3, 0, 0, 0]);
    }
}
