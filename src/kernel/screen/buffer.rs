//! Frame buffer and related

use core::cell::OnceCell;
use core::fmt::Write;

use bootloader_api::info::{FrameBuffer, FrameBufferInfo};
use noto_sans_mono_bitmap::{get_raster, get_raster_width, FontWeight, RasterHeight, RasterizedChar};
use spin::Mutex;

use super::color::{Color, WHITE};

/// Number of untouched pixel around the screen
const SCREEN_PADDING: usize = 0;

/// Number of pixels between two rendered characters
const CHAR_SPACING: usize = 0;

/// Number of pixels between two lines
const LINE_SPACING: usize = 2;

/// Default raster height
const FONT_HEIGHT: RasterHeight = RasterHeight::Size16;

/// Default font style
const FONT_STYLE: FontWeight = FontWeight::Regular;

/// Default height of characters
const CHAR_HEIGHT: usize = FONT_HEIGHT.val();

/// Default width of characters
const CHAR_WIDTH: usize = get_raster_width(FontWeight::Regular, FONT_HEIGHT);

/// Backup character if the one desired is not available in the font
const CHAR_BACKUP: char = '\u{fffd}';

/// Structure used to write characters on the screen
#[derive(Debug)]
pub struct Buffer {
    /// Raw bytes of the frame buffer
    frame_buffer: &'static mut [u8],

    /// Infos on the writer
    info: FrameBufferInfo,

    /// Position on the x-axis in pixels
    ///
    /// The x-axis is directed from left to right
    x: usize,

    /// Position on the x-axis in pixels
    ///
    /// The y-axis is directed from top to bottom
    y: usize,

    /// Current loaded color
    ///
    /// Its purpose is to avoid the heap allocation when calling `print!`
    current_color: Color,
}

impl Buffer {
    /// Creates a new writer from a frame buffer given by the bootloader
    pub fn new(frame_buffer: &'static mut FrameBuffer) -> Self {
        let info = frame_buffer.info();
        let frame_buffer = frame_buffer.buffer_mut();
        let mut buffer = Self {
            frame_buffer,
            info,
            x: SCREEN_PADDING,
            y: SCREEN_PADDING,
            current_color: WHITE,
        };

        buffer.clear();
        buffer
    }

    /// The height of the buffer in pixels
    const fn height(&self) -> usize {
        self.info.height
    }

    /// The width of the buffer in pixels
    const fn width(&self) -> usize {
        self.info.width
    }

    /// Changes the color that is currently loaded
    pub const fn set_color(&mut self, color: Color) {
        self.current_color = color;
    }

    /// Clears the scree with black pixels
    fn clear(&mut self) {
        self.x = SCREEN_PADDING;
        self.y = SCREEN_PADDING;
        self.frame_buffer.fill(0);
    }

    /// Writes a pixel on row `x` and column `y` of color `color`
    ///
    /// # Safety
    ///
    /// Must ensure that the wanted byte is really accessible
    fn write_pixel(&mut self, x: usize, y: usize, color: Color) {
        let bytes_per_pixel = self.info.bytes_per_pixel;
        let byte_offset = (y * self.width() + x) * bytes_per_pixel;

        let color_bytes = color.to_bytes(self.info.pixel_format);
        for (index, color_byte) in color_bytes.iter().enumerate() {
            // SAFETY: This private function never fetch off-screen pixels
            let byte = unsafe { self.frame_buffer.get_unchecked_mut(byte_offset + index) };
            byte.clone_from(color_byte);
        }
    }

    /// Writes a rendered char (given by a matrix of intensity) to the frame buffer
    ///
    /// Before the call, the pointer is located at the top-left corner of the character and moves after the call to the position of
    /// the next char
    fn write_rendered_char(&mut self, char: &RasterizedChar, color: Color) {
        let bytes = char.raster();
        for (row_index, row) in bytes.iter().enumerate() {
            for (col_index, intensity) in row.iter().enumerate() {
                self.write_pixel(self.x + col_index, self.y + row_index, color.with_intensity(*intensity));
            }
        }

        self.x += char.width() + CHAR_SPACING;
    }

    /// Manages new lines
    fn new_line(&mut self) {
        self.y += CHAR_HEIGHT + LINE_SPACING;
        self.carriage_return();

        // If this is the last line of the screen, clear it
        if self.y + CHAR_HEIGHT + LINE_SPACING >= self.height() {
            self.clear();
        }
    }

    /// Manages carriage return
    fn carriage_return(&mut self) {
        self.x = SCREEN_PADDING;
    }
}

impl Write for Buffer {
    fn write_char(&mut self, c: char) -> core::fmt::Result {
        match c {
            '\r' => self.carriage_return(),
            '\n' => self.new_line(),
            _ => {
                // If the current line will overflow, go to the next one
                if self.x + CHAR_WIDTH >= self.width() {
                    self.new_line();
                }

                let raster = get_raster(c, FONT_STYLE, FONT_HEIGHT).unwrap_or_else(|| {
                    get_raster(CHAR_BACKUP, FONT_STYLE, FONT_HEIGHT)
                        .unwrap_or_else(|| unreachable!("The backup character should always be available in the font"))
                });

                self.write_rendered_char(&raster, self.current_color);
            },
        }

        Ok(())
    }

    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        s.chars().try_for_each(|char| self.write_char(char))
    }
}

/// Global writer used to display on the screen
pub static WRITER: Mutex<OnceCell<Buffer>> = Mutex::new(OnceCell::new());

#[cfg(test)]
mod test {
    use core::fmt::Write;

    use super::WRITER;

    #[test_case]
    fn write_str() {
        let mut writer = WRITER.lock();

        writer
            .get_mut()
            .unwrap()
            .write_str("This is a test")
            .expect("Could not write on the screen");
    }

    #[test_case]
    fn write_many_str() {
        let mut writer = WRITER.lock();

        for _ in 0..200_usize {
            writer.get_mut().unwrap().write_str("Test").expect("Could not write on the screen");
        }
    }
}
