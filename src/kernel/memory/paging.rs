//! All things related to paging implementation

use bootloader_api::info::{MemoryRegionKind, MemoryRegions};
use derive_more::{Deref, DerefMut};
use x86_64::registers::control::Cr3;
use x86_64::structures::paging::{FrameAllocator, FrameDeallocator, PageTable, PhysFrame, RecursivePageTable, Size4KiB, Translate};
use x86_64::{PhysAddr, VirtAddr};

use super::PAGE_SIZE;
use crate::mutex::Locked;

/// Struct used to create a global mapper
///
/// Bridge between physical page [`PhysFrame`] and virtual page [`Page`](x86_64::structures::paging::Page)
#[derive(Debug, Deref, DerefMut)]
pub struct Mapper(Option<RecursivePageTable<'static>>);

impl Mapper {
    /// Returns an empty mapper
    pub const fn new() -> Self {
        Self(None)
    }

    /// Initializes the mapper by creating a static offset page table
    ///
    /// # Safety
    ///
    /// Must guarantee that the given `level_4_table` is well defined
    #[inline]
    #[allow(unused)] // Is used but Rust does not see it
    pub unsafe fn init(&mut self, level_4_table: VirtAddr) {
        let (active_level_4_table, _) = Cr3::read();
        self.0 = Some(
            RecursivePageTable::new(level_4_table.as_mut_ptr::<PageTable>().as_mut().unwrap_or_else(|| {
                unreachable!("The cast to `PageTable` is valid as it is done immediately after being cast from a `VirtAddr`")
            }))
            .unwrap_or_else(|_| unreachable!("The level 4 table comes from the bootloader API so it is assumed to be valid")),
        );
    }
}

/// Global memory mapper
pub static MAPPER: Locked<Mapper> = Locked::new(Mapper::new());

impl Translate for Locked<Mapper> {
    fn translate(&self, addr: VirtAddr) -> x86_64::structures::paging::mapper::TranslateResult {
        self.lock().as_mut().expect("The mapper is not initialized !").translate(addr)
    }
}

/// Structure used to manipulate physical frames for the memory mapper
#[derive(Clone, Debug)]
pub struct PhysFrameAllocator {
    /// Memory regions given by the `bootinfo`
    memory_regions: &'static MemoryRegions,

    /// Index of the next memory that will be taken
    next_index: usize,
}

impl PhysFrameAllocator {
    /// Creates a frame allocator with boot informations
    pub const fn new(memory_regions: &'static MemoryRegions) -> Self {
        Self {
            memory_regions,
            next_index: 0,
        }
    }

    /// Returns an iterator over all usable memory regions
    #[allow(clippy::cast_possible_truncation)]
    fn usable_memory_regions(&self) -> impl Iterator<Item = PhysFrame> {
        // All regions
        let regions = self.memory_regions.iter();

        // Filter to retain only usable regions
        let usable_regions = regions.filter(|region| region.kind == MemoryRegionKind::Usable);

        // Map to have the usable address ranges
        let addr_ranges = usable_regions.map(|region| region.start..region.end);

        // Flatten and creation of blocks of `PAGE_SIZE` size
        let frame_addresses = addr_ranges.flat_map(|range| range.step_by(PAGE_SIZE as usize));

        frame_addresses.map(|addr| PhysFrame::containing_address(PhysAddr::new(addr)))
    }
}

// SAFETY: Used frames cannot be taken
unsafe impl FrameAllocator<Size4KiB> for PhysFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame<Size4KiB>> {
        let frame = self.usable_memory_regions().nth(self.next_index);
        self.next_index += 1;
        frame
    }
}

impl FrameDeallocator<Size4KiB> for PhysFrameAllocator {
    // SAFETY: In this current implementation, this does nothing
    unsafe fn deallocate_frame(&mut self, _frame: PhysFrame<Size4KiB>) {}
}

#[cfg(test)]
mod test {
    use x86_64::structures::paging::mapper::TranslateResult;
    use x86_64::structures::paging::Translate;
    use x86_64::VirtAddr;

    use super::MAPPER;

    #[test_case]
    fn map_virtual_address() {
        assert!(matches!(MAPPER.translate(VirtAddr::new(0x000b_8000)), TranslateResult::NotMapped));
        assert!(matches!(MAPPER.translate(VirtAddr::new(0x0020_1008)), TranslateResult::Mapped { .. }));
        assert!(matches!(MAPPER.translate(VirtAddr::new(0x0100_0020_1a10)), TranslateResult::Mapped { .. }));
    }
}
