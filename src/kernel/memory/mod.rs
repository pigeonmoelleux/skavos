//! Everything that touch the memory

mod allocator;
pub mod paging;
pub mod vmm;

use log::info;
use x86_64::structures::paging::mapper::MapToError;
use x86_64::structures::paging::{FrameAllocator, Mapper, Page, PageSize, PageTableFlags, Size4KiB};
use x86_64::VirtAddr;

use self::allocator::FreeList;
use self::paging::MAPPER;
use crate::mutex::Locked;

/// Arbitrary start pointer of the heap
const HEAP_START: usize = 0x_4444_4444_0000;

/// Initial size of the heap
///
/// The heap size will increase dynamically when needed
const HEAP_SIZE: usize = 100 * 1024; // 100 kiB

/// Size of pages in bytes (4096 B = 4.096 kB = 4 kiB)
pub const PAGE_SIZE: u64 = Size4KiB::SIZE;

/// Global allocator of the kernel
#[global_allocator]
pub static ALLOCATOR: Locked<FreeList> = Locked::new(FreeList::new());

/// Aligns the address `addr` upwards to alignment `align`
#[inline]
#[must_use]
pub const fn align_up(addr: usize, align: usize) -> usize {
    let remainder = addr % align;
    if remainder == 0 { addr } else { addr - remainder + align }
}

/// Initializes heap allocation
///
/// Returns the starting address of the last page allocated
pub fn init<FA: FrameAllocator<Size4KiB>>(frame_allocator: &mut FA) -> Result<VirtAddr, MapToError<Size4KiB>> {
    let heap_start = VirtAddr::new(HEAP_START as u64);
    let heap_end = heap_start + HEAP_SIZE - 1_u64;

    let heap_start_page: Page<Size4KiB> = Page::containing_address(heap_start);
    let heap_end_page: Page<Size4KiB> = Page::containing_address(heap_end);
    let pages = Page::range_inclusive(heap_start_page, heap_end_page);

    let last_beginning_addr = pages
        .last()
        .unwrap_or_else(|| unreachable!("The page range is non-empty so it is possible to take the last page"))
        .start_address();

    for page in pages {
        let Some(frame) = frame_allocator.allocate_frame() else {
            return Err(MapToError::FrameAllocationFailed)
        };
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;

        // SAFETY: This operation is safe because the frame page allocator is well defined
        unsafe {
            match MAPPER
                .lock()
                .as_mut()
                .unwrap_or_else(|| panic!("Tried to initialize the heap allocator before the memory mapper"))
                .map_to(page, frame, flags, frame_allocator)
            {
                Ok(mapping) => mapping.flush(),
                Err(err) => return Err(err),
            };
        };
    }

    // SAFETY: Those hardcoded constants works
    unsafe {
        ALLOCATOR.lock().init(HEAP_START, HEAP_SIZE);
    };

    info!("Global allocator initialized");

    Ok(last_beginning_addr)
}
