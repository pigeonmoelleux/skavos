//! Implementation of the virtual memory manager

use alloc::vec::Vec;
use core::fmt::Debug;
use core::hash::{Hash, Hasher};
use core::iter::zip;

use ::alloc::vec;
use derive_more::{Deref, DerefMut};
use hashbrown::{HashMap, HashSet};
use spin::Mutex;
use x86_64::structures::paging::frame::PhysFrameRangeInclusive;
use x86_64::structures::paging::page::PageRangeInclusive;
use x86_64::structures::paging::{FrameAllocator, Mapper, Page, PageSize, PageTableFlags, PhysFrame, Size4KiB};
use x86_64::{PhysAddr, VirtAddr};

use super::paging::{PhysFrameAllocator, MAPPER};

/// Virtual memory region
#[derive(Debug)]
struct VirtualMemoryRegion {
    /// Pages corresponding to the virtual memory region
    region: PageRangeInclusive,
}

impl Hash for VirtualMemoryRegion {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.region.start.start_address().hash(state);
        self.region.end.start_address().hash(state);
    }
}

/// Structure used to manipulate virtual pages for the virtual memory manager
#[derive(Clone, Debug)]
struct VirtualPageAllocator {
    /// Beginning address of the next page that can be allocated
    next: VirtAddr,

    /// Memory regions that have already been allocated but that are free
    available_regions: HashSet<VirtAddr>,
}

impl VirtualPageAllocator {
    /// Creates a new page allocator from the beginning address of the last page allocated
    ///
    /// # Safety
    ///
    /// Must ensure that the given address correspond to the start address of a page
    unsafe fn new(last_beginning_addr: VirtAddr) -> Self {
        Self {
            next: last_beginning_addr + Size4KiB::SIZE,
            available_regions: HashSet::new(),
        }
    }

    /// Returns, if it exists, the start address of an allocatable virtual memory region of `n` pages length
    fn find_region(&self, n: u64) -> Option<VirtAddr> {
        for addr in self.available_regions.iter() {
            let mut pages_available_counter = 0_u64;

            for i in 0..n {
                if self.available_regions.contains(&(*addr + i * Size4KiB::SIZE)) {
                    pages_available_counter += 1;
                };
            }

            if pages_available_counter == n {
                return Some(*addr);
            }
        }

        None
    }

    /// Tries to allocate a new page and returns it if it succeeded
    fn allocate_page(&mut self) -> Page<Size4KiB> {
        match self.find_region(1) {
            None => {
                // SAFETY: the alignment of `self.next` is ensure everywhere in the implementation
                let page = unsafe { Page::from_start_address_unchecked(self.next) };
                self.next += Size4KiB::SIZE;
                page
            },
            Some(start_addr) => {
                self.available_regions.remove(&start_addr);
                Page::from_start_address(start_addr)
                    .unwrap_or_else(|_| unreachable!("Only starting addresses are present in `available_region`"))
            },
        }
    }

    /// Tries to allocate `n` new consecutive pages
    fn allocate_region(&mut self, n: u64) -> PageRangeInclusive<Size4KiB> {
        match self.find_region(n) {
            None => {
                let pages = Page::range_inclusive(
                    // SAFETY: the alignment of `self.next` is ensure everywhere in the implementation
                    unsafe { Page::from_start_address_unchecked(self.next) },
                    // SAFETY: the alignment of `self.next` is ensure everywhere in the implementation, thus `self.next + <int> *
                    // <Size of pages>` is also aligned
                    unsafe { Page::from_start_address_unchecked(self.next + (n - 1) * Size4KiB::SIZE) },
                );
                self.next += n * Size4KiB::SIZE;
                pages
            },
            Some(start_addr) => {
                self.available_regions.remove(&start_addr);
                Page::range_inclusive(
                    // SAFETY: all addresses in `self.available_regions` are starting addresses
                    unsafe { Page::from_start_address_unchecked(start_addr) },
                    // SAFETY: all addresses in `self.available_regions` are starting addresses, thus `self.next + <int> *
                    // <Size of pages>` is also aligned
                    unsafe { Page::from_start_address_unchecked(start_addr + (n - 1) * Size4KiB::SIZE) },
                )
            },
        }
    }

    /// Deallocates an allocated page
    ///
    /// # Panics
    ///
    /// Panics if tries to deallocate a page already free
    fn deallocate_page(&mut self, page: Page<Size4KiB>) {
        assert!(
            self.available_regions.insert(page.start_address()),
            "Tries to deallocate the page {page:?} that have already been deallocated"
        );
    }
}

/// Structure used to keep trace of mappings
#[derive(Clone, Debug, Default)]
struct Mappings {
    /// Retrieve a physical address from a virtual address
    virt_to_phys: HashMap<VirtAddr, PhysAddr>,

    /// Retrieve virtual addresses from a physical address
    phys_to_virt: HashMap<PhysAddr, Vec<VirtAddr>>,
}

impl Mappings {
    /// Add a mapping
    ///
    /// # Panics
    ///
    /// Panics if the given page is already mapped
    fn map(&mut self, page: Page<Size4KiB>, frame: PhysFrame) {
        assert!(
            self.virt_to_phys.insert(page.start_address(), frame.start_address()).is_none(),
            "Tried to map a page already mapped"
        );
        match self.phys_to_virt.get_mut(&frame.start_address()) {
            None => {
                let phys_mappings = vec![page.start_address()];
                self.phys_to_virt.insert(frame.start_address(), phys_mappings);
            },
            Some(phys_mappings) => phys_mappings.push(page.start_address()),
        };
    }

    /// Return the list of pages mapped to a frame
    fn frame_to_pages(&self, frame: PhysFrame) -> Option<Vec<Page<Size4KiB>>> {
        let Some(phys_mappings) = self.phys_to_virt
                .get(&frame.start_address()) else { return None; };

        Some(
            phys_mappings
                .iter()
                .map(|virt_addr|
                    // SAFETY: it is ensure everywhere in the implementation that the mappings only contains starting addresses
                    unsafe { Page::from_start_address_unchecked(*virt_addr) })
                .collect(),
        )
    }

    /// Remove `frame` from mappings
    ///
    /// # Panics
    ///
    /// Panics if `page` is not present in mappings
    fn unmap_frame(&mut self, frame: PhysFrame) {
        let phys_mappings = self
            .phys_to_virt
            .remove(&frame.start_address())
            .expect("Tried to unmap a not-mapped frame");
        for virt_addr in phys_mappings {
            self.virt_to_phys
                .remove(&virt_addr)
                .unwrap_or_else(|| unreachable!("It is ensure that `phys_to_virt` and `virt_to_phys` contain the same elements"));
        }
    }

    /// Remove `page` from mappings
    ///
    /// # Panics
    ///
    /// Panics if `page` is not present in mappings
    fn unmap_page(&mut self, page: Page<Size4KiB>) {
        let Some(phys_start_address) = self.virt_to_phys.remove(&page.start_address()) else { panic!("Tried to unmap a not-mapped page") };
        let Some(phys_mappings) = self.phys_to_virt.get_mut(&phys_start_address) else { unreachable!("If the physical frame is mapped, then there is at least one virtual page pointing to this frame") };
        phys_mappings.remove(
            phys_mappings
                .iter()
                .position(|addr| addr == &page.start_address())
                .unwrap_or_else(|| unreachable!("It is ensure that `phys_to_virt` and `virt_to_phys` contain the same elements")),
        );
    }
}

/// Virtual memory manager
#[derive(Clone, Debug)]
pub struct VirtualMemoryManager {
    /// Physical frame allocator
    frame_allocator: PhysFrameAllocator,

    /// Virtual page allocator
    page_allocator: VirtualPageAllocator,

    /// Keeps the mappings between physical and virtual memory
    mappings: Mappings,
}

impl VirtualMemoryManager {
    /// Creates a new virtual memory manager
    ///
    /// The last beginning address is the virtual beginning address of the last allocated page
    ///
    /// # Safety
    ///
    /// Must ensure that the given beginning address corresponds to the start address of a page
    pub unsafe fn new(frame_allocator: PhysFrameAllocator, last_beginning_addr: VirtAddr) -> Self {
        Self {
            frame_allocator,
            page_allocator: VirtualPageAllocator::new(last_beginning_addr),
            mappings: Mappings::default(),
        }
    }

    /// Converts a physical address to the list of virtual addresses it is mapped on
    #[allow(dead_code)]
    pub fn translate_phys_addr(&self, phys_addr: PhysAddr) -> Vec<VirtAddr> {
        let frame = PhysFrame::<Size4KiB>::containing_address(phys_addr);
        let pages_start_addr = self.mappings.phys_to_virt.get(&frame.start_address()).unwrap_or(&Vec::new()).clone();
        pages_start_addr
            .into_iter()
            .map(|virt_addr| virt_addr + (phys_addr - frame.start_address()))
            .collect()
    }

    /// Maps a `page` with a `frame` with given `flags`
    ///
    /// # Safety
    ///
    /// The safety conditions are the same as the one for `x86_64::structures::paging::mapper::Mapper::map_to`
    pub unsafe fn map(&mut self, page: Page<Size4KiB>, frame: PhysFrame, flags: PageTableFlags) -> Option<Page<Size4KiB>> {
        let mut binding = MAPPER.lock();
        let Some(mapper) = binding.as_mut() else { return None; };
        // SAFETY: the frame and page allocators are well-defined and errors are handled before this point
        match unsafe { mapper.map_to(page, frame, flags, &mut self.frame_allocator) } {
            Ok(mapping) => {
                mapping.flush();
                self.mappings.map(page, frame);
                Some(page)
            },
            Err(_) => None,
        }
    }

    /// Returns a new page if possible
    #[allow(dead_code)]
    pub fn new_page(&mut self, flags: PageTableFlags) -> Option<Page<Size4KiB>> {
        let Some(frame) = self.frame_allocator.allocate_frame() else { return None; };
        let page = self.page_allocator.allocate_page();

        // SAFETY: the virtual memory manager is well defined
        unsafe { self.map(page, frame, flags) }
    }

    /// Returns a new page mapped to a given `frame`
    #[allow(dead_code)]
    pub fn new_page_from_frame(&mut self, frame: PhysFrame, flags: PageTableFlags) -> Option<Page<Size4KiB>> {
        let page = self.page_allocator.allocate_page();

        // SAFETY: the virtual memory manager is well defined
        unsafe { self.map(page, frame, flags) }
    }

    /// Returns a new page range mapped to a given frame range
    pub fn new_page_range_from_frame_range(
        &mut self,
        frame_range: PhysFrameRangeInclusive,
        flags: PageTableFlags,
    ) -> Option<PageRangeInclusive<Size4KiB>> {
        let page_range = self.page_allocator.allocate_region(frame_range.count() as u64);

        for (page, frame) in zip(page_range, frame_range) {
            // SAFETY: the virtual memory manager is well defined
            let Some(_) = (unsafe { self.map(page, frame, flags) }) else {
                return None;
            };
        }

        Some(page_range)
    }

    /// Returns a new page range if possible
    #[allow(dead_code)]
    pub fn new_page_range(&mut self, flags: PageTableFlags, n: u64) -> Option<PageRangeInclusive<Size4KiB>> {
        let pages = self.page_allocator.allocate_region(n);

        let mut binding = MAPPER.lock();
        let Some(mapper) = binding.as_mut() else { return None; };
        for page in pages {
            let Some(frame) = self.frame_allocator.allocate_frame() else { return None; };

            // SAFETY: the frame and page allocators are well-defined and errors are handled before this point
            match unsafe { mapper.map_to(page, frame, flags, &mut self.frame_allocator) } {
                Ok(mapping) => {
                    mapping.flush();
                    self.mappings.map(page, frame);
                },
                Err(_) => return None,
            };
        }
        Some(pages)
    }

    /// Returns a new page range if possible
    ///
    /// The pages won't be managed by the virtual memory manager, so use instead `new_page_range` when it is possible
    pub fn new_unmanaged_page_range(&mut self, flags: PageTableFlags, n: u64) -> Option<PageRangeInclusive<Size4KiB>> {
        let pages = self.page_allocator.allocate_region(n);

        let mut binding = MAPPER.lock();
        let Some(mapper) = binding.as_mut() else { return None; };
        for page in pages {
            let Some(frame) = self.frame_allocator.allocate_frame() else { return None; };

            // SAFETY: the frame and page allocators are well-defined and errors are handled before this point
            match unsafe { mapper.map_to(page, frame, flags, &mut self.frame_allocator) } {
                Ok(mapping) => mapping.flush(),
                Err(_) => return None,
            };
        }
        Some(pages)
    }

    /// Unmaps a frame that have been allocated
    ///
    /// # Panics
    ///
    /// Panics if the `MAPPER` cannot unmap the frame
    pub fn unmap_frame(&mut self, frame: PhysFrame) {
        for page in self.mappings.frame_to_pages(frame).expect("Tried to unmap a not-mapped frame") {
            let (_, flush) = MAPPER
                .lock()
                .as_mut()
                .expect("The memory mapper is not initialized")
                .unmap(page)
                .unwrap_or_else(|unmap_error| panic!("Failed to unmap the page {page:#?} : {unmap_error:?}"));
            flush.flush();
        }
        self.mappings.unmap_frame(frame);
    }

    /// Unmaps a page that have been allocated
    ///
    /// If `keep_physical` is false, all the mapped pages on the physical frame will be unmaped also
    ///
    /// # Panics
    ///
    /// Panics if the `MAPPER` cannot unmap the page
    pub fn unmap_page(&mut self, page: Page<Size4KiB>, keep_physical: bool) {
        let (frame, flush) = MAPPER
            .lock()
            .as_mut()
            .expect("The memory mapper is not initialized")
            .unmap(page)
            .unwrap_or_else(|unmap_error| panic!("Failed to unmap the page {page:?}: {unmap_error:?}"));
        flush.flush();
        self.page_allocator.deallocate_page(page);
        self.mappings.unmap_page(page);
        if !keep_physical {
            self.unmap_frame(frame);
        }
    }

    /// Unmaps a frame range that have been allocated
    ///
    /// # Panics
    ///
    /// Panics if the `MAPPER` cannot unmap one of the frames
    #[allow(dead_code)]
    pub fn unmap_frame_range(&mut self, frames: PhysFrameRangeInclusive) {
        for frame in frames {
            self.unmap_frame(frame);
        }
    }

    /// Unmaps a page range that have been allocated
    ///
    /// If `keep_physical` is false, all the mapped pages on the physical frame will be unmaped also
    #[allow(dead_code)]
    pub fn unmap_page_range(&mut self, pages: PageRangeInclusive<Size4KiB>, keep_physical: bool) {
        for page in pages {
            self.unmap_page(page, keep_physical);
        }
    }
}

/// Structure used to define a global Virtual Memory Manager
#[derive(Debug, Deref, DerefMut)]
pub struct Vmm(Option<VirtualMemoryManager>);

impl Vmm {
    /// Initializes the global virtual memory manager
    ///
    /// # Safety
    ///
    /// Must ensure that the given beginning address corresponds to the start address of a page
    pub unsafe fn init(&mut self, frame_allocator: PhysFrameAllocator, last_beginning_addr: VirtAddr) {
        self.0 = Some(
            // SAFETY: this is safe if the function's safety condition is verified
            unsafe { VirtualMemoryManager::new(frame_allocator, last_beginning_addr) },
        );
    }
}

#[allow(clippy::non_send_fields_in_send_ty)]
// SAFETY: this structure will only be used behind a mutex
unsafe impl Send for Vmm {}

/// Global virtual memory manager
pub static VIRTUAL_MEMORY_MANAGER: Mutex<Vmm> = Mutex::new(Vmm(None));

#[cfg(test)]
mod test {
    use alloc::vec::Vec;

    use spin::{Lazy, Mutex};
    use x86_64::structures::paging::{Page, PageTableFlags, PhysFrame, Size4KiB};
    use x86_64::{PhysAddr, VirtAddr};

    use super::{VirtualPageAllocator, VIRTUAL_MEMORY_MANAGER};

    const BEGINNING_ADDR: u64 = 0x_4444_4444_0000;
    static PAGE_ALLOCATOR: Lazy<Mutex<VirtualPageAllocator>> = Lazy::new(|| {
        Mutex::new(
            // SAFETY: this is safe as the constant has already been tested
            unsafe { VirtualPageAllocator::new(VirtAddr::new(BEGINNING_ADDR)) },
        )
    });

    #[test_case]
    fn local_page_allocation() {
        PAGE_ALLOCATOR.lock().allocate_page();
    }

    #[test_case]
    fn local_page_range_allocation() {
        let mut page_allocator = PAGE_ALLOCATOR.lock();

        let page_range = page_allocator.allocate_region(1);
        assert_eq!(page_range.count(), 1);

        let page_range = page_allocator.allocate_region(5);
        assert_eq!(page_range.count(), 5);
    }

    #[test_case]
    fn local_page_deallocation() {
        let mut page_allocator = PAGE_ALLOCATOR.lock();
        let page = page_allocator.allocate_page();
        page_allocator.deallocate_page(page);
    }

    #[test_case]
    fn local_page_range_deallocation() {
        let mut page_allocator = PAGE_ALLOCATOR.lock();
        let page_range = page_allocator.allocate_region(5);

        for page in page_range {
            page_allocator.deallocate_page(page);
        }
    }

    #[test_case]
    fn global_page_allocation() {
        VIRTUAL_MEMORY_MANAGER.lock().as_mut().unwrap().new_page(PageTableFlags::PRESENT).unwrap();
    }

    #[test_case]
    fn global_page_deallocation() {
        let mut binding = VIRTUAL_MEMORY_MANAGER.lock();
        let vmm = binding.as_mut().unwrap();

        let page = vmm.new_page(PageTableFlags::PRESENT).unwrap();
        vmm.unmap_page(page, true);

        let page = vmm.new_page(PageTableFlags::PRESENT).unwrap();
        vmm.unmap_page(page, false);
    }

    #[test_case]
    fn global_page_range_allocation() {
        let mut binding = VIRTUAL_MEMORY_MANAGER.lock();
        let vmm = binding.as_mut().unwrap();

        let page_range = vmm.new_page_range(PageTableFlags::PRESENT, 5).unwrap();
        let start_addresses: Vec<VirtAddr> = page_range.map(Page::start_address).collect();

        for i in 0..5 {
            for j in i + 1..5 {
                assert_ne!(
                    vmm.mappings.virt_to_phys.get(&start_addresses[i]).unwrap(),
                    vmm.mappings.virt_to_phys.get(&start_addresses[j]).unwrap()
                );
            }
        }
    }

    #[test_case]
    fn translate_phys_addr() {
        let mut binding = VIRTUAL_MEMORY_MANAGER.lock();
        let vmm = binding.as_mut().unwrap();

        let frame = PhysFrame::<Size4KiB>::from_start_address(PhysAddr::new(0x0eb9_a000)).unwrap();
        let page = vmm
            .new_page_from_frame(frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE)
            .unwrap();

        assert_eq!(&vmm.translate_phys_addr(frame.start_address() + 0x234_u64)[0], &(page.start_address() + 0x234_u64));
    }
}
