//! Free list allocator implementation

use core::alloc::{GlobalAlloc, Layout};
use core::{mem, ptr};

use x86_64::structures::paging::PageTableFlags;

use super::vmm::VIRTUAL_MEMORY_MANAGER;
use super::{align_up, PAGE_SIZE};
use crate::mutex::Locked;

/// Memory area that can be allocated
///
/// Used to know if it is allocated or not
#[derive(Debug)]
struct AllocatableArea {
    /// Size of the area (in bytes)
    size: usize,

    /// Next allocatable area (if it exists)
    next: Option<&'static mut Self>,
}

impl AllocatableArea {
    /// Creates a new allocatable area
    const fn new(size: usize) -> Self {
        Self { size, next: None }
    }

    /// Returns the starting address of the area
    fn start_addr(&self) -> usize {
        self as *const Self as usize
    }

    /// Returns the ending address of the area
    fn end_addr(&self) -> usize {
        self.start_addr() + self.size
    }
}

/// Linked list allocator structure
#[derive(Debug)]
pub struct FreeList {
    /// Current allocatable area
    head: AllocatableArea,
}

impl FreeList {
    /// Creates a new free list structure
    pub const fn new() -> Self {
        Self {
            head: AllocatableArea::new(0),
        }
    }

    /// Initializes a free list
    ///
    /// # SAFETY
    ///
    /// Must ensure that the given address range of the heap is unused
    /// Should only be called once
    #[allow(unused)]
    pub unsafe fn init(&mut self, heap_start: usize, heap_size: usize) {
        self.add_free_area(heap_start, heap_size);
    }

    /// Adds the area beginning at `start_addr` of `size` to the free list
    ///
    /// # Safety
    ///
    /// Must ensure that the given address range of the heap is unused
    ///
    /// # Panics
    ///
    /// Panics if `start_addr` is not aligned with the one of [`AllocatableArea`] or if the given size if smaller than the size of
    /// [`AllocatableArea`]
    unsafe fn add_free_area(&mut self, start_addr: usize, size: usize) {
        // Ensures that the area is able to hold a `FreeList`
        assert_eq!(
            align_up(start_addr, mem::align_of::<AllocatableArea>()),
            start_addr,
            "Cannot add a free area with a unaligned starting address"
        );
        assert!(
            size >= mem::size_of::<AllocatableArea>(),
            "Cannot add a free area with a size smaller than the size of the free list structure"
        );

        // Creates a new allocatable area and appends it at the start of the free list
        let mut area = AllocatableArea::new(size);
        area.next = self.head.next.take();
        let area_ptr = ptr::from_exposed_addr_mut::<AllocatableArea>(start_addr);
        area_ptr.write(area);
        self.head.next = Some(&mut *area_ptr);
    }

    /// Tries to use the given area for an allocation with given size and alignment
    ///
    /// Returns the starting address on success
    fn try_allocate_area(area: &AllocatableArea, size: usize, align: usize) -> Option<usize> {
        let start_addr = align_up(area.start_addr(), align);
        let Some(end_addr) = start_addr.checked_add(size) else { return None; };

        if end_addr > area.end_addr() {
            None
        } else {
            let remainder = area.end_addr() - end_addr;
            if remainder > 0 && remainder < mem::size_of::<AllocatableArea>() { None } else { Some(start_addr) }
        }
    }

    /// Finds and returns the first allocatable area with at least given size and alignment then removes it from the list
    ///
    /// Returns, if it exists, a tuple of this area and the starting address
    #[allow(clippy::cast_possible_truncation)]
    fn find_area(&mut self, size: usize, align: usize) -> Option<(&'static AllocatableArea, usize)> {
        let mut head = &mut self.head;

        while let Some(ref mut area) = head.next {
            let Some(start_addr) = Self::try_allocate_area(area, size, align) else {
                // SAFETY: `area.next` is `Some` so it is safe to unwrap
                head = unsafe { head.next.as_mut().unwrap_unchecked() };

                continue;
            };

            let next_area = area.next.take();
            let Some(allocatable_area) = head.next.take() else { return None; };

            head.next = next_area;

            return Some((allocatable_area as &'static _, start_addr));
        }

        let mut binding = VIRTUAL_MEMORY_MANAGER.lock();
        // This exception can occurres if the starting heap size is not big enough
        let vmm = binding.as_mut().expect("The virtual memory manager has not been initialized yet");

        // This size ensure that there will be enough place to allocate
        let size_to_allocate = size + mem::size_of::<AllocatableArea>() + mem::align_of::<AllocatableArea>();
        let pages_to_allocate = size_to_allocate as u64 / PAGE_SIZE + u64::from(size_to_allocate as u64 % PAGE_SIZE != 0);
        // Have to use `new_unmanaged_page_range` and not `new_page_range` to avoid dead lock
        let page_range = vmm.new_unmanaged_page_range(PageTableFlags::PRESENT | PageTableFlags::WRITABLE, pages_to_allocate)?;

        let start_addr = align_up(page_range.start.start_address().as_u64() as usize, mem::align_of::<AllocatableArea>());
        let size_allocated = (pages_to_allocate * PAGE_SIZE) as usize;
        let area = AllocatableArea::new(size_allocated);
        let area_ptr = ptr::from_exposed_addr_mut::<AllocatableArea>(start_addr);

        // SAFETY: the pointer is at the beginning of the freshly created allocatable area
        unsafe {
            area_ptr.write(area);
        };

        // SAFETY: the area pointed has not bee altered since the last operation
        Some((unsafe { &mut *area_ptr }, start_addr))
    }

    /// Returns the size and the alignment adjusted so that the allocated area can store an `AllocatedArea`
    fn size_align(layout: Layout) -> Option<(usize, usize)> {
        layout
            .align_to(mem::align_of::<AllocatableArea>())
            .map_or(None, |layout| Some((layout.size().max(mem::size_of::<AllocatableArea>()), layout.pad_to_align().align())))
    }
}

// SAFETY: free lists with mutually exclusive access are safe iff allocations and deallocations are safe
unsafe impl GlobalAlloc for Locked<FreeList> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let Some((size, align)) = FreeList::size_align(layout) else {
            return ptr::null_mut()
        };

        let mut allocator = self.lock();

        if let Some((area, start_addr)) = allocator.find_area(size, align) {
            let Some(end_addr) = start_addr.checked_add(size) else { return ptr::null_mut(); };
            let remainder = area.end_addr() - end_addr;

            if remainder > mem::size_of::<AllocatableArea>() + mem::align_of::<AllocatableArea>() {
                allocator.add_free_area(align_up(end_addr, mem::align_of::<AllocatableArea>()), remainder);
            }

            ptr::from_exposed_addr_mut(start_addr)
        } else {
            ptr::null_mut()
        }
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        assert_eq!(
            align_up(ptr.addr(), mem::align_of::<AllocatableArea>()),
            ptr.addr(),
            "The given pointer to dealloc is not aligned"
        );

        let Some((size, _)) = FreeList::size_align(layout) else {
            unreachable!("All the condition  required in `Layout::from_size_align` are respected in the call of `FreeList::size_align`")
        };

        self.lock().add_free_area(ptr.addr(), size);
    }
}

#[cfg(test)]
mod test {
    use alloc::boxed::Box;
    use alloc::vec::Vec;

    use super::AllocatableArea;

    #[test_case]
    fn allocatable_area() {
        let allocatable_area = AllocatableArea::new(100);
        assert_eq!(allocatable_area.size, 100);
        assert!(allocatable_area.next.is_none());
    }

    #[test_case]
    fn alloc() {
        let boxed = Box::new(5);
        assert_eq!(5_usize, *boxed);
    }

    #[test_case]
    #[allow(clippy::vec_init_then_push)] // This is the goal of this test
    fn multiple_alloc() {
        let mut vec = Vec::with_capacity(1);
        vec.push(1_usize);
        vec.push(1_usize);
    }

    #[test_case]
    fn increase_heap_size() {
        let mut vec = Vec::<u64>::with_capacity(1_000_000);
        vec.push(1);
    }
}
