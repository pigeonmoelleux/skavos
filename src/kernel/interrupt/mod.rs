//! Everything that touch exceptions
//!
//! Contains CPU exceptions, interruptions, double faults, etc

mod acpi;
// mod aml;
// mod apic;
pub mod cmos;
mod handler;
pub mod keyboard;
mod pics;
pub mod pit;
mod stack;

use ::acpi::platform::interrupt::Apic;
use ::acpi::platform::ProcessorInfo;
use ::acpi::AcpiTables;
use conquer_once::spin::OnceCell;
use handler::IDT;
use log::debug;
use stack::GDT_SELECTORS;
use x86_64::instructions::interrupts;
use x86_64::instructions::tables::load_tss;
use x86_64::registers::segmentation::{Segment, CS, DS, ES, FS, GS, SS};

use self::acpi::Mapper;
use self::pics::PICS;

/// Global ACPI tables
pub static ACPI_TABLES: OnceCell<AcpiTables<Mapper>> = OnceCell::uninit();

/// Global AML table
// pub static AML: OnceCell<Wrapper> = OnceCell::uninit();

/// Global local APIC
pub static APIC: OnceCell<Apic> = OnceCell::uninit();

/// Hardware processor info
pub static PROCESSOR_INFO: OnceCell<Option<ProcessorInfo>> = OnceCell::uninit();

/// Initialize all structures linked to interruptions
#[inline]
pub fn init(rsdp_addr: u64) {
    // Initialization of the GDT
    GDT_SELECTORS.gdt.load();

    // SAFETY: this operation is safe as code segment selectors are well defined
    unsafe {
        // Reloading of the code segment register
        CS::set_reg(GDT_SELECTORS.code);
    };

    // SAFETY: this operation is safe as code segment selectors are well defined
    unsafe {
        DS::set_reg(GDT_SELECTORS.data);
    };

    // SAFETY: this operation is safe as code segment selectors are well defined
    unsafe {
        ES::set_reg(GDT_SELECTORS.data);
    };

    // SAFETY: this operation is safe as code segment selectors are well defined
    unsafe {
        FS::set_reg(GDT_SELECTORS.data);
    };

    // SAFETY: this operation is safe as code segment selectors are well defined
    unsafe {
        GS::set_reg(GDT_SELECTORS.data);
    };

    // SAFETY: this operation is safe as code segment selectors are well defined
    unsafe {
        SS::set_reg(GDT_SELECTORS.data);
    };

    // SAFETY: this operation is safe as TSS segment selectors are well defined
    unsafe {
        // Loading of our TSS segment
        load_tss(GDT_SELECTORS.tss);
    };

    debug!("GDT loaded and initialized");

    // Initialization of the keyboard
    keyboard::init();

    debug!("Keyboard initialized");

    // Initialization of the IDT
    IDT.load();

    debug!("IDT loaded");

    // SAFETY: the offsets are constant and work
    unsafe {
        PICS.lock().initialize();
    };

    debug!("GDT loaded");

    // Enables CPU interrupts
    interrupts::enable();

    // Initializes the PIT
    pit::init();

    debug!("PIT initialized");

    // Initialization of the ACPI
    ACPI_TABLES.init_once(|| acpi::init(rsdp_addr).unwrap_or_else(|acpi_error| panic!("ACPI error: {acpi_error:?}")));

    debug!("ACPI tables initialized");

    // Initialization of the APIC (currently using the PIC)
    // apic::init();
}
