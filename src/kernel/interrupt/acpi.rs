//! Framework to use the Advanced Programmable Interrupt Controller (APIC)

use core::ptr::NonNull;
use core::sync::atomic::{AtomicU8, Ordering};

use acpi::fadt::Fadt;
use acpi::madt::Madt;
use acpi::sdt::Signature;
use acpi::{AcpiError, AcpiHandler, AcpiTables, InterruptModel, PhysicalMapping, RsdpError};
use x86_64::structures::paging::frame::PhysFrameRangeInclusive;
use x86_64::structures::paging::{PageTableFlags, PhysFrame};
use x86_64::PhysAddr;

use super::{APIC, PROCESSOR_INFO};
use crate::kernel::memory::vmm::VIRTUAL_MEMORY_MANAGER;

/// Cached `ACPI enable` register value
static ACPI_ENABLE: AtomicU8 = AtomicU8::new(u8::MAX);

/// Cached `ACPI disable` register value
static ACPI_DISABLE: AtomicU8 = AtomicU8::new(u8::MAX);

/// Minimal structure used to implement an `acpi::AcpiHandler`
#[derive(Clone, Debug)]
pub struct Mapper;

impl AcpiHandler for Mapper {
    #[allow(clippy::cast_possible_truncation)]
    unsafe fn map_physical_region<T>(&self, physical_address: usize, size: usize) -> PhysicalMapping<Self, T> {
        let frame_range = PhysFrameRangeInclusive {
            start: PhysFrame::containing_address(PhysAddr::new(physical_address as u64)),
            end: PhysFrame::containing_address(PhysAddr::new((physical_address + size) as u64)),
        };
        let page_range = VIRTUAL_MEMORY_MANAGER
            .lock()
            .as_mut()
            .expect("The virtual memory manager has not been initialized yet")
            .new_page_range_from_frame_range(frame_range, PageTableFlags::PRESENT | PageTableFlags::WRITABLE)
            .expect("Could not allocate and map a new page range");
        let length = (page_range.start.size() as usize) * page_range.count();
        PhysicalMapping::new(
            physical_address,
            NonNull::new(page_range.start.start_address().as_mut_ptr())
                .unwrap_or_else(|| unreachable!("The page starting address cannot be 0")),
            length,
            length,
            self.clone(),
        )
    }

    // This function should not be called but it is so there is nothing in it
    fn unmap_physical_region<T>(_region: &PhysicalMapping<Self, T>) {}
}

/// Initializes the ACPI
///
/// # Panics
///
/// Panics if unable to read the APCI tables
#[allow(clippy::cast_possible_truncation)] // Target system is 64-bits
#[allow(clippy::similar_names)] // FADT and MADT are common names
pub(super) fn init(rsdp_addr: u64) -> Result<AcpiTables<Mapper>, AcpiError> {
    let handler = Mapper {};
    // SAFETY: this operation is safe because the `rsdp_addr` is returned by the `bootloader_api`
    let Ok(tables) = (unsafe { AcpiTables::from_rsdp(handler, rsdp_addr as usize) }) else { return Err(AcpiError::Rsdp(RsdpError::NoValidRsdp)); };

    // SAFETY: the ACPI tables are well-defined
    let Ok(Some(fadt)) = (unsafe { tables.get_sdt::<Fadt>(Signature::FADT) }) else { return Err(AcpiError::TableMissing(Signature::FADT)); };
    fadt.validate()?;
    ACPI_ENABLE.store(fadt.acpi_enable, Ordering::Relaxed);
    ACPI_DISABLE.store(fadt.acpi_disable, Ordering::Relaxed);

    // TODO: repair DSDT
    // let dsdt = tables.dsdt.as_ref().ok_or(AcpiError::TableMissing(Signature::DSDT))?;
    // AML.init_once(|| aml::Wrapper::new(dsdt)); // Triple fault here

    // SAFETY: the ACPI tables are well defined
    let Ok(Some(madt)) = (unsafe { tables.get_sdt::<Madt>(Signature::MADT) }) else { return Err(AcpiError::TableMissing(Signature::MADT)); };
    let (interrupt_model, processor_info) = madt.parse_interrupt_model()?;
    let InterruptModel::Apic(apic) = interrupt_model else { unreachable!("It is assumed that the interrupt model is based on ACPI") };

    APIC.init_once(|| apic);
    PROCESSOR_INFO.init_once(|| processor_info);

    Ok(tables)
}
