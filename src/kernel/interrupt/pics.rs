//! Implementation of the 8259 Programmable Interrupt Controller (PIC)
//!
//! See more informations on the [OSdev wiki](https://wiki.osdev.org/PIC)

use pic8259::ChainedPics;
use spin::Mutex;

/// Offset of the master PIC
pub(super) const PIC1_OFFSET: u8 = 0x20;

/// Offset of the slave PIC (should always be [`PIC1_OFFSET + 8`](PIC1_OFFSET))
pub(super) const PIC2_OFFSET: u8 = 0x28;

/// PICs 8259 controller
pub(super) static PICS: Mutex<ChainedPics> = Mutex::new(
    // SAFETY: the offsets are constants and work
    unsafe { ChainedPics::new(PIC1_OFFSET, PIC2_OFFSET) },
);
