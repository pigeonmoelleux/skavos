//! Implementation of the Programmable Intveral Timer (PIT) and more generally of everything related to time
//!
//! See more details on the [OSdev wiki](https://wiki.osdev.org/Programmable_Interval_Timer)

use core::sync::atomic::{AtomicUsize, Ordering};

use x86_64::instructions::port::Port;
use x86_64::instructions::{self, interrupts};

use super::cmos::CMOS;

/// Frequency of the PIT
///
/// This is the most possibly precise value described here : [OSdev wiki](https://wiki.osdev.org/Programmable_Interval_Timer#Mode_2_.E2.80.93_Rate_Generator)
#[allow(dead_code)]
const FREQUENCY: f64 = 3_579_545.0 / 3.0;

/// Divider for PIT
const DIVIDER: u32 = 0x0001_0000;

/// Time in seconds between successive ticks
#[allow(dead_code)]
const INTERVAL: f64 = DIVIDER as f64 / FREQUENCY;

/// Output channel for the PIT frequency divider
const OUTPUT_CHANNEL: u8 = 0;

/// Ticks elapsed since PIT was initialized.
pub(super) static TICKS: AtomicUsize = AtomicUsize::new(0);

/// The latest RTC clock update tick.
pub static LAST_RTC_UPDATE: AtomicUsize = AtomicUsize::new(0);

/// Initializes the PIT
pub(super) fn init() {
    set_frequency_divider(OUTPUT_CHANNEL);

    // Enables RTC interrupts
    CMOS.enable_interrupt();
}

/// Set the PIT's frequency divider
///
/// See more informations on the [OSdev wiki](https://wiki.osdev.org/Programmable_Interval_Timer#I.2FO_Ports)
fn set_frequency_divider(channel: u8) {
    /// Returns the port corresponding to a channel
    const fn channel_to_port(channel: u8) -> u16 {
        match channel {
            0 => 0x40,
            1 => 0x41,
            2 => 0x42,
            _ => panic!("The output channel of the PIT frequency divider should be between 0 and 2 included"),
        }
    }

    // This part is inspired by the example code found on the [OSdev wiki](https://wiki.osdev.org/Programmable_Interval_Timer#PIT_Channel_0_Example_Code)
    interrupts::without_interrupts(|| {
        /// Mode/Command register (write-only)
        const CMD_REGISTER: u16 = 0x43;

        /// Channel bit in the [`CMD_REGISTER`]
        const CHANNEL_BIT: u8 = 0x06;

        /// Command access mode set in mode lobyte/hibyte
        const ACCESS_MODE: u16 = 0x30;

        /// Command operating mode set in mode rate generator (see the [OSdev wiki](https://wiki.osdev.org/Programmable_Interval_Timer#Mode_2_.E2.80.93_Rate_Generator))
        const OPERATING_MODE: u16 = 0x06;

        let channel_mask: u16 = (channel << CHANNEL_BIT).into();

        // 65536 = 0x0001_0000 is a special value described [here](https://wiki.osdev.org/Programmable_Interval_Timer#Setting_The_Reload_Value)
        let bytes = (if DIVIDER == 0x0001_0000 { 0 } else { DIVIDER }).to_le_bytes();

        let mut cmd = Port::new(CMD_REGISTER);
        let mut data = Port::new(channel_to_port(channel));

        // SAFETY: this operation does not has any memory violation side effects
        unsafe {
            cmd.write(channel_mask | ACCESS_MODE | OPERATING_MODE);
        };

        for byte in bytes {
            // SAFETY: this operation does not has any memory violation side effects
            unsafe {
                data.write(byte);
            };
        }
    });
}

/// Returns the number of ticks elapsed since the initialization of the PIT
pub(super) fn ticks() -> usize {
    TICKS.load(Ordering::Relaxed)
}

/// Returns the uptime (time between the initialization of the PIT and now)
///
/// As this function manipulates floats, using it is discouraged.
#[allow(clippy::cast_precision_loss)]
#[allow(clippy::float_arithmetic)]
pub fn uptime() -> f64 {
    INTERVAL * (ticks() as f64)
}

/// Halts the CPU for a given duration
///
/// As this function manipulates floats, using it is discouraged.
#[allow(clippy::float_arithmetic)] // There is now other way to sleep properly
#[allow(dead_code)]
pub fn sleep(milliseconds: u32) {
    let start = uptime();
    while uptime() - start < (milliseconds / 1000).into() {
        instructions::hlt();
    }
}

#[cfg(test)]
#[allow(clippy::float_arithmetic)]
mod test {
    use super::*;

    #[test_case]
    fn sleep_1s() {
        let uptime_begin = uptime();
        sleep(1000);
        assert!(uptime() - uptime_begin > 1_f64);
    }

    #[test_case]
    fn uptime_non_zero() {
        assert!(uptime() > 0_f64);
    }
}
