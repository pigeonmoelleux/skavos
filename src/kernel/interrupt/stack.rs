//! Contains all structures used for the stack
//!
//! Here are defined the TSS (Table Task Segment) and the GDT (Global Descriptor Table).

use spin::Lazy;
use x86_64::structures::gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector};
use x86_64::structures::tss::TaskStateSegment;

/// Global task state segment
static TSS: TaskStateSegment = TaskStateSegment::new();

/// Structure grouping the GDT and two segment selectors
pub struct GDTSelectors {
    /// Global descriptor table
    pub gdt: GlobalDescriptorTable,

    /// Segment selector of the kernel code
    pub code: SegmentSelector,

    /// Segment selector of the kernel data
    pub data: SegmentSelector,

    /// Segment selector of the TSS
    pub tss: SegmentSelector,
}

/// Global global descriptor table with code and TSS segment selectors
pub(super) static GDT_SELECTORS: Lazy<GDTSelectors> = Lazy::new(|| {
    let mut gdt = GlobalDescriptorTable::new();
    let code = gdt.add_entry(Descriptor::kernel_code_segment());
    let data = gdt.add_entry(Descriptor::kernel_data_segment());
    let tss = gdt.add_entry(Descriptor::tss_segment(&TSS));
    GDTSelectors {
        gdt,
        code,
        data,
        tss,
    }
});
