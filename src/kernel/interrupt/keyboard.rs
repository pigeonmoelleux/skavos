//! Handling keyboards inputs

use core::cell::OnceCell;
use core::pin::Pin;
use core::sync::atomic::{AtomicBool, Ordering};
use core::task::{Context, Poll};

use crossbeam_queue::ArrayQueue;
use derive_more::{Deref, DerefMut};
use futures_util::task::AtomicWaker;
use futures_util::Stream;
use log::{warn, Level};
use pc_keyboard::layouts::{Azerty, Us104Key};
use pc_keyboard::{DecodedKey, Error, HandleControl, KeyCode, KeyEvent, KeyState, Keyboard, ScancodeSet1};
use spin::Mutex;
use x86_64::instructions::port::Port;

use crate::encoding::ascii::Char;
use crate::syslog::SYS_LOGGER;

/// Keyboard layout of the device used
const KEYBOARD_LAYOUT: LayoutWrapper =
    LayoutWrapper::Azerty(Keyboard::new(ScancodeSet1::new(), Azerty, HandleControl::MapLettersToUnicode));

/// Is the ALT key pressed ?
pub static ALT_PRESSED: AtomicBool = AtomicBool::new(false);

/// Is the CTRL key pressed ?
pub static CTRL_PRESSED: AtomicBool = AtomicBool::new(false);

/// Is the SHIFT key pressed ?
pub static SHIFT_PRESSED: AtomicBool = AtomicBool::new(false);

/// Keyboard layouts
pub enum LayoutWrapper {
    /// AZERTY layout
    Azerty(Keyboard<Azerty, ScancodeSet1>),

    /// QWERTY layout
    #[allow(dead_code)]
    Qwerty(Keyboard<Us104Key, ScancodeSet1>),
}

impl LayoutWrapper {
    /// Wrapper around [`pc_keyboard::Keyboard::add_byte`]
    pub fn add_byte(&mut self, byte: u8) -> Result<Option<KeyEvent>, Error> {
        match self {
            Self::Azerty(keyboard) => keyboard.add_byte(byte),
            Self::Qwerty(keyboard) => keyboard.add_byte(byte),
        }
    }

    /// Wrapper around [`pc_keyboard::Keyboard::process_keyevent`]
    pub fn process_keyevent(&mut self, event: KeyEvent) -> Option<DecodedKey> {
        match self {
            Self::Azerty(keyboard) => keyboard.process_keyevent(event),
            Self::Qwerty(keyboard) => keyboard.process_keyevent(event),
        }
    }
}

/// Gloabal keyboard interface
pub static KEYBOARD: Mutex<OnceCell<LayoutWrapper>> = Mutex::new(OnceCell::new());

/// Reads the keyboard's input port
///
/// See the [OSdev wiki](https://wiki.osdev.org/"8042"_PS/2_Controller#PS.2F2_Controller_IO_Ports) for more informations
pub fn get_scancode() -> u8 {
    /// Input port number of the keyboard interruptions
    const PORT: u16 = 0x60;

    let mut port = Port::new(PORT);

    // SAFETY: this is safe since the port number used is the one specified in the doc
    unsafe { port.read() }
}

/// Stream of pressed keys
#[derive(Debug, Deref, DerefMut)]
struct KeyboardStream(ArrayQueue<char>);

impl KeyboardStream {
    /// Creates a new keyboard stream with a fixed capacity
    fn new() -> Self {
        Self(ArrayQueue::new(100))
    }
}

/// Static waker used for the implementation of [`futures_util::stream::Stream`] for [`KeyboardStream`]
static WAKER: AtomicWaker = AtomicWaker::new();

/// Global stream of pressed keys as scanned codes
static KEY_QUEUE: conquer_once::spin::OnceCell<KeyboardStream> = conquer_once::spin::OnceCell::uninit();

/// Wrapper around [`KeyboardStream`] to define a global queue for keyboard inputs
pub struct KeyQueue(());

impl Stream for KeyQueue {
    type Item = char;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        if let Some(scan_code) = KEY_QUEUE.get().expect("Tried to use the key queue before its initialization").pop() {
            return Poll::Ready(Some(scan_code));
        };

        WAKER.register(cx.waker());
        KEY_QUEUE
            .get()
            .expect("Tried to use the key queue before its initialization")
            .pop()
            .map_or(Poll::Pending, |scan_code| {
                WAKER.take();
                Poll::Ready(Some(scan_code))
            })
    }
}

impl KeyQueue {
    /// Creates a new key queue wrapper
    ///
    /// Every [`KeyQueue`] are linked to a unique queue
    pub const fn new() -> Self {
        Self(())
    }

    /// Adds a new char to the queue
    fn push_key(c: char) {
        if KEY_QUEUE.get().expect("Tried to use the keyboard before its initialization").push(c) == Ok(()) {
            WAKER.wake();
        } else {
            log::log!(Level::Error, "Warning: the scan code queue is full");
        }
    }

    /// Sends a Control Sequence Introducer (CSI) to the console
    ///
    /// See the [OSdev wiki](https://wiki.osdev.org/Terminals#Receiving_Keys) for the reference
    fn send_csi(code: &'static str) {
        Self::push_key(Char::ESC.into());
        Self::push_key(Char::BracketOpen.into());
        for byte in code.bytes() {
            Self::push_key(byte.into());
        }
    }

    /// Handles key returned by the interrupt handler `handle_interrupt`
    ///
    /// Keys that return a standard character are sent directly to the queue
    ///
    /// Arrows, `TAB` and `DEL` are sent with CSI
    ///
    /// Moreover, the following keys combinations are used :
    /// - `CTRL` + `ALT` + `DEL`: reboots the computer (not implemented yet)
    /// - `ALT` + `L`: flushes the logger
    #[allow(clippy::wildcard_enum_match_arm)]
    pub fn handle_key(key: DecodedKey) {
        /// Tabulation char
        const TAB: char = Char::Tab.into_char();

        /// Delete char
        const DEL: char = Char::DEL.into_char();

        let alt_pressed = ALT_PRESSED.load(Ordering::Relaxed);
        let ctrl_pressed = CTRL_PRESSED.load(Ordering::Relaxed);
        let shift_pressed = SHIFT_PRESSED.load(Ordering::Relaxed);

        match key {
            DecodedKey::RawKey(KeyCode::ArrowUp) => Self::send_csi("1A"),
            DecodedKey::RawKey(KeyCode::ArrowDown) => Self::send_csi("1B"),
            DecodedKey::RawKey(KeyCode::ArrowRight) => Self::send_csi("1C"),
            DecodedKey::RawKey(KeyCode::ArrowLeft) => Self::send_csi("1D"),
            DecodedKey::Unicode(TAB) if shift_pressed => Self::send_csi("2"),
            DecodedKey::Unicode(DEL) if alt_pressed && ctrl_pressed => warn!("TODO: implement reboot"),
            DecodedKey::Unicode('l') if alt_pressed => {
                let mut logger = SYS_LOGGER.lock();
                logger.display_messages();
                logger.clear();
            },
            DecodedKey::Unicode(key) => Self::push_key(key),
            _ => {},
        }
    }

    /// Interrupt handler
    ///
    /// This function is called by the IDT after a keyboard interruption
    #[allow(clippy::wildcard_enum_match_arm)]
    pub fn handle_interrupt() {
        let mut binding = KEYBOARD.lock();
        let keyboard = binding.get_mut().expect("Tried to get the keyboard before its initialization");

        let scancode = get_scancode();
        let Ok(Some(key_event)) = keyboard.add_byte(scancode) else { return; };
        match key_event.code {
            KeyCode::LAlt | KeyCode::RAltGr => ALT_PRESSED.store(key_event.state == KeyState::Down, Ordering::Relaxed),
            KeyCode::LShift | KeyCode::RShift => SHIFT_PRESSED.store(key_event.state == KeyState::Down, Ordering::Relaxed),
            KeyCode::LControl | KeyCode::RControl => SHIFT_PRESSED.store(key_event.state == KeyState::Down, Ordering::Relaxed),
            _ => {},
        };

        if let Some(key) = keyboard.process_keyevent(key_event) {
            Self::handle_key(key);
        };
    }
}

/// Initializes the keyboard
pub fn init() {
    let keyboard = KEYBOARD.lock();
    keyboard
        .set(KEYBOARD_LAYOUT)
        .unwrap_or_else(|_| panic!("Could not initialize the keyboard"));

    KEY_QUEUE.init_once(KeyboardStream::new);
}
