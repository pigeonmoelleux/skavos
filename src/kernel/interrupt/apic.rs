//! Implementation of Advanced Programmable Interrupt Controller (APIC)
//!
//! To initializes the APIC, it is needed to initialize the PIC then to disable it.

use x86::apic::xapic::XAPIC_SVR;
use x86::msr::APIC_BASE;
use x86_64::registers::model_specific::Msr;
use x86_64::structures::paging::{PageTableFlags, PhysFrame};
use x86_64::PhysAddr;

use super::APIC;
use crate::kernel::memory::vmm::VIRTUAL_MEMORY_MANAGER;

/// Initializes the local APIC
///
/// Must ensure that [`APIC`] is initialized
///
/// The algorithm used is inspired on the example that can be found [here](https://wiki.osdev.org/APIC#Local_APIC_configuration)
pub(super) fn init() {
    // unimplemented!("Replace the PIC with the local APIC");

    // Currently, the PICs is used and not the APIC
    // // SAFETY : the PIC is well-defined
    // unsafe {
    //    PICS.lock().disable();
    // };

    let mut msr = Msr::new(APIC_BASE);

    // SAFETY: there is no unsafe side effect
    let cur = unsafe { msr.read() };

    // SAFETY: there is no unsafe side effect
    unsafe {
        msr.write(cur | 0x800); // Set bit 11
    };

    let vmm = VIRTUAL_MEMORY_MANAGER
        .lock()
        .as_mut()
        .expect("The virtual memory manager is not initialized");

    let apic = APIC.get().unwrap_or_else(|| unreachable!("The APIC has not been initialized"));
    println!("{:?}", apic);
    let frame = PhysFrame::containing_address(PhysAddr::new(apic.local_apic_address));
    let page = vmm
        .new_page_from_frame(frame, PageTableFlags::PRESENT | PageTableFlags::WRITABLE)
        .expect("Could not allocate a new page");

    // Initialization of the APIC by setting the spurious interrupt vector
    // See the [OSdev wiki](https://wiki.osdev.org/APIC#Spurious_Interrupt_Vector_Register) for more informations
    let tgt = ((page.start_address() + (apic.local_apic_address - frame.start_address().as_u64())).as_u64()
        + Into::<u64>::into(XAPIC_SVR)) as usize;

    // SAFETY: source and destination addresses are well-formed
    unsafe {
        core::ptr::write_volatile(tgt as *mut u32, 0x100 | 0xFF);
    };
}
