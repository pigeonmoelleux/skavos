//! Implementation of the Complementary Metal-Oxide Semiconductor (CMOS)
//!
//! See [stanislavs.org](https://stanislavs.org/helppc/cmos_ram.html) and the [OSdev wiki](https://wiki.osdev.org/CMOS) for more details

use core::fmt::Display;
use core::hint::spin_loop;

use x86_64::instructions::interrupts;
use x86_64::instructions::port::Port;

/// Real-time clock
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Rtc {
    /// Year (between 0 and 99)
    pub year: u16,

    /// Month (between 1 and 12)
    pub month: u8,

    /// Day (between 1 and 31)
    pub day: u8,

    /// Day of week (between 1 (Sunday) and 7 (Saturday))
    #[allow(dead_code)]
    pub dow: u8,

    /// Month (between 0 and 23)
    pub hour: u8,

    /// Minute (between 0 and 59)
    pub minute: u8,

    /// Second (between 0 and 59)
    pub second: u8,
}

/// Century number
const CENTURY: u16 = 20;

impl Display for Rtc {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}{}/{}/{} {}:{}:{}", CENTURY, self.year, self.month, self.day, self.hour, self.minute, self.second)
    }
}

/// CMOS registers
///
/// See the [OSdev wiki](https://wiki.osdev.org/CMOS#Getting_Current_Date_and_Time_from_RTC) for more details
#[repr(u8)]
#[derive(Debug, Clone)]
enum Register {
    /// Second register
    Second = 0x00,

    /// Minute register
    Minute = 0x02,

    /// Hour register
    ///
    /// Between 0 and 23 in 24-hour mode, and 1 and 12 in 12-hour mode with the highest bit set if pm
    Hour = 0x04,

    /// Weekday register
    ///
    /// Sunday = 1
    Weekday = 0x06,

    /// Day register
    Day = 0x07,

    /// Month register
    Month = 0x08,

    /// Year register
    Year = 0x09,

    /// Status register A
    A = 0x0A,

    /// Status register B
    B = 0x0B,

    /// Status register B
    C = 0x0C,
}

impl From<Register> for u8 {
    fn from(value: Register) -> Self {
        value as Self
    }
}

/// CMOS structure
///
/// Can only be accessed through IO ports 0x70 and 0x71
pub struct Cmos {
    /// Address port
    addr: Port<u8>,

    /// Data port
    data: Port<u8>,
}

/// Global CMOS
pub const CMOS: Cmos = Cmos::new();

/// Address port
const ADDR_PORT: u16 = 0x70;

/// Data port
const DATA_PORT: u16 = 0x71;

impl Cmos {
    /// Creates a new CMOS
    const fn new() -> Self {
        Self {
            addr: Port::new(ADDR_PORT),
            data: Port::new(DATA_PORT),
        }
    }

    /// Reads a value from the given register
    fn read_register(&mut self, register: Register) -> u8 {
        // SAFETY: this operation cannot violate memory
        unsafe {
            self.addr.write(register.into());
        };

        // SAFETY: this operation cannot violate memory
        unsafe { self.data.read() }
    }

    /// Writes a given value into a given register
    fn write_register(&mut self, register: Register, value: u8) {
        // SAFETY: this operation cannot violate memory
        unsafe {
            self.addr.write(register.into());
        };

        // SAFETY: this operation cannot violate memory
        unsafe {
            self.data.write(value);
        };
    }

    /// Enables Non-Maskable Inputs (NMI)
    fn enable_nmi(&mut self) {
        /// 011111111
        const MASK: u8 = 0x7F;

        // SAFETY: this operation cannot violate memory
        let nmi = unsafe { self.addr.read() };

        // SAFETY: this operation cannot violate memory
        unsafe {
            self.addr.write(nmi | MASK);
        };
    }

    /// Disables Non-Maskable Inputs (NMI)
    fn disable_nmi(&mut self) {
        /// 10000000
        const MASK: u8 = 0x80;

        // SAFETY: this operation cannot violate memory
        let nmi = unsafe { self.addr.read() };

        // SAFETY: this operation cannot violate memory
        unsafe {
            self.addr.write(nmi | MASK);
        };
    }

    /// Notifies the end of an interrupt
    ///
    /// See the [OSdev wiki](https://wiki.osdev.org/RTC#Interrupts_and_Register_C) for more details
    pub fn notify_end_of_interrupt(&mut self) {
        self.read_register(Register::C);
    }

    /// Enables CMOS interruptions
    ///
    /// See the [OSdev wiki](https://wiki.osdev.org/RTC#Turning_on_IRQ_8) for more details
    pub fn enable_interrupt(&mut self) {
        interrupts::without_interrupts(|| {
            self.disable_nmi();
            let byte = self.read_register(Register::B);
            self.write_register(Register::B, byte | 0x40);
            self.enable_nmi();
            self.notify_end_of_interrupt();
        });
    }

    /// Returns a raw Real-Time Clock
    fn rtc_raw(&mut self) -> Rtc {
        Rtc {
            second: self.read_register(Register::Second),
            minute: self.read_register(Register::Minute),
            hour: self.read_register(Register::Hour),
            day: self.read_register(Register::Day),
            dow: self.read_register(Register::Weekday),
            month: self.read_register(Register::Month),
            year: self.read_register(Register::Year).into(),
        }
    }

    /// Is the CMOS currently updating ?
    fn is_updating(&mut self) -> bool {
        /// 10000000
        const MASK: u8 = 0x80;

        self.read_register(Register::A) & MASK != 0
    }

    /// Waits until the CMOS is updated
    fn wait_while_updating(&mut self) {
        while self.is_updating() {
            spin_loop();
        }
    }

    /// Returns the current Real-Time Clock
    pub fn rtc(&mut self) -> Rtc {
        /// Believe in the docs
        const SRB_BCD_MODE: u8 = 0x04;

        /// Believe in the docs
        const SRB_H12_MODE: u8 = 0x02;

        /// Believe in the docs
        const HOUR_H12_FMT: u8 = 0x80;

        // Weird bit manipulation that is explained [here](https://wiki.osdev.org/CMOS#Format_of_Bytes) in the OS Dev Wiki
        let bcd_to_binary = |bcd| -> u8 { ((bcd & 0xF0) >> 1_u8) + ((bcd & 0xF0) >> 3_usize) + (bcd & 0x0F) };
        let h12_to_h24 = |h12| -> u8 { ((h12 & 0x7F) + 12_u8) % 24_u8 };

        let mut rtc;

        // Explanation on the [OSdev wiki](https://wiki.osdev.org/CMOS#RTC_Update_In_Progress)
        loop {
            self.wait_while_updating();
            rtc = self.rtc_raw();
            self.wait_while_updating();
            if rtc == self.rtc_raw() {
                break;
            }
        }

        let status_reg_b = self.read_register(Register::B);

        // Convert BCD to binary.
        if status_reg_b & SRB_BCD_MODE == 0 {
            rtc.second = bcd_to_binary(rtc.second);
            rtc.minute = bcd_to_binary(rtc.minute);
            rtc.hour = bcd_to_binary(rtc.hour);
            rtc.day = bcd_to_binary(rtc.day);
            rtc.month = bcd_to_binary(rtc.month);
            rtc.year = bcd_to_binary(
                rtc.year
                    .try_into()
                    .unwrap_or_else(|_| unreachable!("At least 200 years can pass before needing an u16 encoding")),
            )
            .into();
        }

        // Convert 12H to 24H.
        if (status_reg_b & SRB_H12_MODE == 0) && (rtc.hour & HOUR_H12_FMT == 0) {
            rtc.hour = h12_to_h24(rtc.hour);
        }

        // Add century.
        rtc.year += CENTURY;

        rtc
    }
}
