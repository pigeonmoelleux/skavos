//! Implementation of task waker

use alloc::sync::Arc;
use alloc::task::Wake;
use core::task::Waker;

use crossbeam_queue::SegQueue;

use super::TaskId;

/// Waker structure for [`Task`](super::Task)
#[derive(Debug)]
pub(super) struct WakerWrapper {
    /// Task identifier
    id: TaskId,

    /// Current task queue
    queue: Arc<SegQueue<TaskId>>,
}

impl Wake for WakerWrapper {
    fn wake(self: Arc<Self>) {
        self.queue.push(self.id);
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.queue.push(self.id);
    }
}

impl From<WakerWrapper> for Waker {
    fn from(value: WakerWrapper) -> Self {
        Self::from(Arc::new(value))
    }
}

impl WakerWrapper {
    /// Creates a new task waker
    pub(super) fn new(id: TaskId, queue: Arc<SegQueue<TaskId>>) -> Self {
        Self { id, queue }
    }
}

#[cfg(test)]
mod test {
    use alloc::sync::Arc;
    use alloc::task::Wake;

    use crossbeam_queue::SegQueue;

    use super::WakerWrapper;
    use crate::kernel::task::TaskId;

    #[test_case]
    fn new_waker() {
        let id = TaskId::new();
        let queue = Arc::new(SegQueue::<TaskId>::new());

        let _: WakerWrapper = WakerWrapper::new(id, queue);
    }

    #[test_case]
    fn wake() {
        let id = TaskId::new();
        let queue = Arc::new(SegQueue::<TaskId>::new());
        let waker = Arc::new(WakerWrapper::new(id, queue));
        waker.wake();
    }
}
