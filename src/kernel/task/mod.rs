//! Everything related to tasks and multitasking

pub mod executor;
mod waker;

use alloc::boxed::Box;
use core::fmt::Debug;
use core::future::Future;
use core::pin::Pin;
use core::sync::atomic::{AtomicU64, Ordering};
use core::task::{Context, Poll};

/// Next unique task identifier
static NEXT_TASK_ID: AtomicU64 = AtomicU64::new(0);

/// Unique identifier a task
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct TaskId(u64);

impl TaskId {
    /// Returns a new task ID that have never been given
    ///
    /// Since 2^64 > 10^18, `u64::MAX` is expected to never be reached
    fn new() -> Self {
        Self(NEXT_TASK_ID.fetch_add(1, Ordering::Relaxed))
    }
}

/// General task structure
///
/// It is implemented as a wrapper around a [`core::pin::Pin`], head-allocated (with [`alloc::boxed::Box`]) and dispatched
/// [`core::future::Future`] with `Output = ()`
pub struct Task {
    /// Unique identifier
    id: TaskId,

    /// Function that will be executed
    future: Pin<Box<dyn Future<Output = ()>>>,
}

impl Debug for Task {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Task {{ id: {:?} }}", self.id)
    }
}

impl Task {
    /// Creates a new [`Task`] from a [`Future`]
    pub fn new(future: impl Future<Output = ()> + 'static) -> Self {
        Self {
            id: TaskId::new(),
            future: Box::pin(future),
        }
    }

    /// Wrapper around [`core::task::Poll`]
    ///
    /// Tries to resolve the future to a final value, registering the current task for wake up if the value is not yet available
    fn poll(&mut self, cx: &mut Context) -> Poll<()> {
        self.future.as_mut().poll(cx)
    }
}

#[cfg(test)]
mod test {
    use super::{Task, TaskId};
    use crate::print;

    #[test_case]
    fn unique_id() {
        let mut ids = [TaskId::new(); 5];
        for id in &mut ids {
            id.clone_from(&TaskId::new());
        }

        for i in 0..5 {
            for j in 0..i {
                assert_ne!(ids.get(i).unwrap(), ids.get(j).unwrap());
            }
        }
    }

    #[test_case]
    #[allow(clippy::unused_async)]
    fn new_task() {
        async fn foo() {}

        async fn bar() -> usize {
            5
        }

        async fn boo() {
            print!("{}", bar().await);
        }

        let _: Task = Task::new(foo());
        let _: Task = Task::new(boo());
    }
}
