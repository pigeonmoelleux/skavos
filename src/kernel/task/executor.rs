//! Implementation of a simple competitive task executor

use alloc::collections::BTreeMap;
use alloc::sync::Arc;
use core::fmt::Debug;
use core::task::{Context, Poll, Waker};

use crossbeam_queue::SegQueue;
use x86_64::instructions::interrupts;

use super::waker::WakerWrapper;
use super::{Task, TaskId};

/// Task executor using a queue as storage
#[derive(Debug)]
pub struct Executor {
    /// Pending tasks
    tasks: BTreeMap<TaskId, Task>,

    /// Queue ordering the execution of pending tasks
    queue: Arc<SegQueue<TaskId>>,

    /// [`Waker`] associated with tasks
    waker_cache: BTreeMap<TaskId, Waker>,
}

impl Executor {
    /// Creates a new task executor
    pub fn new() -> Self {
        Self {
            tasks: BTreeMap::new(),
            queue: Arc::new(SegQueue::new()),
            waker_cache: BTreeMap::new(),
        }
    }

    /// Adds a task to the pending queue
    ///
    /// # Panics
    ///
    /// Will panic if the task has an ID already given
    pub fn spawn(&mut self, task: Task) {
        let task_id = task.id;
        match self.tasks.insert(task_id, task) {
            Some(_) => panic!("A task with ID {:?} is already registered", task_id),
            None => self.queue.push(task_id),
        }
    }

    /// Halts the CPU if task queue is empty
    fn sleep_if_idle(&self) {
        interrupts::disable();

        if self.queue.is_empty() {
            interrupts::enable_and_hlt();
        } else {
            interrupts::enable();
        }
    }

    /// Runs all the pending tasks
    fn run_once(&mut self) {
        while let Some(task_id) = self.queue.pop() {
            let Some(task) = self.tasks.get_mut(&task_id) else { continue };

            let waker = self
                .waker_cache
                .entry(task_id)
                .or_insert_with(|| WakerWrapper::new(task.id, Arc::<crossbeam_queue::SegQueue<TaskId>>::clone(&self.queue)).into());

            let mut context = Context::from_waker(waker);

            match task.poll(&mut context) {
                Poll::Ready(()) => {
                    self.tasks.remove(&task_id);
                    self.waker_cache.remove(&task_id);
                },
                Poll::Pending => {},
            }
        }
    }

    /// Main task runner
    #[cfg(not(test))]
    pub fn run(&mut self) -> ! {
        loop {
            self.run_once();
            self.sleep_if_idle();
        }
    }

    /// Main task runner
    #[cfg(test)]
    pub fn run(&mut self) {
        while !self.queue.is_empty() {
            self.run_once();
            self.sleep_if_idle();
        }
    }
}

#[cfg(test)]
mod test {

    use conquer_once::spin::OnceCell;

    use super::Executor;
    use crate::kernel::task::Task;

    #[test_case]
    #[allow(clippy::unused_async)]
    fn run_task() {
        async fn foo() {}

        let mut executor = Executor::new();
        executor.spawn(Task::new(foo()));
        executor.run();
    }

    #[test_case]
    #[allow(clippy::unused_async)]
    fn run_multiple_task() {
        static BAR: OnceCell<usize> = OnceCell::uninit();

        async fn foo() {}

        async fn bar() {
            BAR.try_init_once(|| 5).unwrap();
        }

        async fn boo() {
            bar().await;
            assert_eq!(BAR.get().unwrap(), &5);
        }

        let mut executor = Executor::new();
        executor.spawn(Task::new(foo()));
        executor.spawn(Task::new(boo()));
        executor.run();
    }
}
